FROM golang:1.16

RUN mkdir /app
WORKDIR /app

COPY ./ /app/

RUN go build

EXPOSE 8000
CMD /app/pig